#include <errno.h>
#include <linux/if_packet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc,char** argv) {
  if(argc<2){
    fprintf(stderr,"Usage: %s ifname [port]\n",argv[0]);
    return 1;
  }
  char* ifname=argv[1];
  unsigned short port=9;
  if(2<argc){sscanf(argv[2],"%hu",&port);}
  int sendfd=socket(AF_PACKET,SOCK_DGRAM,0);
  if(sendfd<0){
    perror("E_socket(AF_PACKET)");
    return sendfd;
  }
  int recvfd=socket(AF_INET,SOCK_DGRAM,0);
  if(recvfd<0){
    perror("E_socket(AF_INET)");
    return recvfd;
  }
  struct sockaddr_in bind_addr;
  bind_addr.sin_family=AF_INET;
  bind_addr.sin_addr.s_addr=INADDR_ANY;
  bind_addr.sin_port=htons(port);
  int status=bind(recvfd,(struct sockaddr*)&bind_addr,sizeof(bind_addr));
  if(status<0){
    perror("E_bind");
    return status;
  }
  struct sockaddr_ll dest_addr;
  dest_addr.sll_family=AF_PACKET;
  dest_addr.sll_halen=ETH_ALEN;
  __recv:while(1){
    unsigned char buf[102];
    struct sockaddr_in src_addr;
    socklen_t addrlen=sizeof(src_addr);
    ssize_t len=recvfrom(recvfd,buf,sizeof(buf),MSG_WAITALL,
     (struct sockaddr*)&src_addr,&addrlen);
    if(len!=sizeof(buf)
     || buf[0]!=0xFF || buf[1]!=0xFF || buf[2]!=0xFF
     || buf[3]!=0xFF || buf[4]!=0xFF || buf[5]!=0xFF){continue;}
    unsigned char* ptr0=buf+6;
    if(ptr0[0]==0xFF && ptr0[1]==0xFF && ptr0[2]==0xFF
     && ptr0[3]==0xFF && ptr0[4]==0xFF && ptr0[5]==0xFF){continue;}
    unsigned char* ptrX=ptr0+16*ETH_ALEN;
    for (unsigned char* ptr=ptr0+ETH_ALEN;ptr<ptrX;ptr+=ETH_ALEN){
      if(memcmp(ptr,ptr0,ETH_ALEN)){goto __recv;}
    }
    memcpy(dest_addr.sll_addr,ptr0,ETH_ALEN);
    char srcbuf[NI_MAXHOST];
    if(getnameinfo((struct sockaddr*)&src_addr,sizeof(src_addr),srcbuf,sizeof(srcbuf),NULL,0,0)){
      perror("W_getnameinfo");
      srcbuf[0]='?';
      srcbuf[1]=0;
    }
    struct ifreq ifr;
    strncpy(ifr.ifr_name,ifname,sizeof(ifr.ifr_name)-1);
    int status=ioctl(sendfd,SIOCGIFINDEX,&ifr);
    if(!(status<0)){
      dest_addr.sll_ifindex=ifr.ifr_ifindex;
      status=sendto(sendfd,buf,sizeof(buf),0,(struct sockaddr*)&dest_addr,sizeof(dest_addr));
    }
    printf("WOL(%s) %s > %02X:%02X:%02X:%02X:%02X:%02X - %s\n",ifname,srcbuf,
     ptr0[0],ptr0[1],ptr0[2],ptr0[3],ptr0[4],ptr0[5],(status<0)?strerror(errno):"OK");
  }
  close(recvfd);
  close(sendfd);
  return 0;
}
